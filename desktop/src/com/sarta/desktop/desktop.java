package com.sarta.desktop;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.nio.ByteBuffer;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;
import javax.sound.sampled.UnsupportedAudioFileException;

import com.sarta.lib.Utils;

// choppy: http://www.javaprogrammingforums.com/whats-wrong-my-code/7825-streaming-using-udp.html\
// inputstream example: https://forums.oracle.com/forums/thread.jspa?threadID=1690320&tstart=1030

public class Desktop implements Runnable {
	
	public enum Mechanism {
		BUFFER, STREAM
	}
	
	public static final int LISTEN_PORT = 1355;
	private static final int sizeOfLong = Long.SIZE/8;
	private static final int sizeOfShort = Short.SIZE/8; 
	
	private DatagramSocket datagramSocket;
	private DatagramPacket datagramPacket;
	private byte[] buffer;
	private AudioFormat format = null;
	private DataLine.Info dataLineInfo = null; 
	private SourceDataLine sourceDataLine = null;
	private float fmtSampleRate_Android = (float) 44100;
	private int fmtSampleSizeInBits_Android = 16;
	private int fmtChannels_Android = 1;
	private boolean fmtSigned_Android = true;
	private boolean fmtBigEndian_Android = false;	
	
	public static void main(String[] args) throws SocketException {
		Desktop desktop = new Desktop();
		desktop.run();
	}
	
	public Desktop() throws SocketException {
		datagramSocket = new DatagramSocket(LISTEN_PORT);
		buffer = new byte[Utils.BUFFER_SIZE];
		datagramPacket = new DatagramPacket(buffer, buffer.length);
	}
	
	public void run() {
		try {
			start();
		} catch (IOException | UnsupportedAudioFileException | LineUnavailableException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void start() throws IOException, UnsupportedAudioFileException, LineUnavailableException {
		
		System.out.println("Sarta listening on port: " + LISTEN_PORT);
		
		// initialize format
		format = new AudioFormat(fmtSampleRate_Android, fmtSampleSizeInBits_Android, fmtChannels_Android,
						fmtSigned_Android, fmtBigEndian_Android);
		
		// get data line
		dataLineInfo = new DataLine.Info(SourceDataLine.class, format);
	    sourceDataLine = (SourceDataLine) AudioSystem.getLine(dataLineInfo);
		
	    sourceDataLine.open(format);
	    sourceDataLine.start();
	    
		long prevTimeStamp = 0, timeStamp = 0;
		int nPackets = 0;
		while (true) {	
			// read into packet
			System.out.println("Listening...");
			datagramSocket.receive(datagramPacket);
			int packetSize = datagramPacket.getLength();
			//trim buffer
			ByteBuffer buffer = ByteBuffer.wrap(datagramPacket.getData(), 0, packetSize);
			
			nPackets++;
			System.out.println("Received packet " + nPackets + ", size = " + buffer.limit());
						
			timeStamp = buffer.getLong(buffer.limit() - sizeOfLong);
			
			if (timeStamp <= prevTimeStamp) {
				System.out.println("received a packet out of order!");
				System.out.println("timestamp: " + timeStamp + " prev: " + prevTimeStamp);
				continue;
			}
			prevTimeStamp = timeStamp;
			
			System.out.println("numAudioBytes: " + (packetSize - sizeOfLong));
			
			sourceDataLine.write(buffer.array(), 0, packetSize - sizeOfLong);				
						
		}
		
//	    sourceDataLine.drain();
//	    sourceDataLine.close();
	}

}
