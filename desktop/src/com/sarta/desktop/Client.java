package com.sarta.desktop;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;
import javax.sound.sampled.UnsupportedAudioFileException;

import com.sarta.lib.Utils;

public class Client {

	public static final String filePath = "C:\\test.wav";
	public static final Integer BUFFER_SIZE = 5000;
	public static final Integer port = 1355;
	public static final String host = "localhost";
	
	private static boolean play = false;
	
	public static void main(String[] args) throws UnsupportedAudioFileException, IOException, LineUnavailableException {
		
		byte[] buffer = new byte[BUFFER_SIZE];
		
		// network
		InetAddress address =InetAddress.getByName(host);
		DatagramSocket socket = new DatagramSocket();
		DatagramPacket packet;
		
		// audio
		File soundFile = new File(filePath);
		AudioInputStream audioStream = AudioSystem.getAudioInputStream( soundFile );
		AudioFormat audioFormat = audioStream.getFormat();
		System.out.println( "input audio format=" + audioFormat );
		
		if (play) {
			// for playing only 
	        DataLine.Info info = new DataLine.Info(SourceDataLine.class, audioFormat);
	        SourceDataLine sourceLine = (SourceDataLine) AudioSystem.getLine(info);
	        sourceLine.open(audioFormat);
	        sourceLine.start();
	        int nBytesRead = 0, nBytesWritten = 0;
	        while (nBytesRead != -1) {
	    	
		    	// read into buffer
		        nBytesRead = audioStream.read(buffer, 0, buffer.length);
		        
		        // play audio
		        if (nBytesRead >= 0) {
		            nBytesWritten = sourceLine.write(buffer, 0, nBytesRead);
		        }
	        }
	        sourceLine.drain();
	        sourceLine.close();
		}
		
		// see http://stackoverflow.com/questions/6933920/extracting-bytes-from-audio-file-java
		
		ByteArrayOutputStream byteOut = new ByteArrayOutputStream();
		AudioSystem.write(audioStream, AudioFileFormat.Type.WAVE, byteOut);
		audioStream.close();
		byteOut.close();
		    
      	byte[] dataBytes = byteOut.toByteArray();
        System.out.println("size: " + dataBytes.length);
      	
        byte[][] toSend = Utils.splitByteArray(dataBytes, BUFFER_SIZE - Utils.sizeOfLong);
        for (int i = 0; i < toSend.length; i++) {
        	byte[] thisArr = toSend[i];
        	
            buffer = Utils.addTimeBytes(thisArr);        	
        	
        	// send through network
	        packet = new DatagramPacket(buffer, buffer.length, address, port);
	        System.out.println("Sending packet " + (i+1) + " of " + toSend.length);
			socket.send(packet);
			System.out.println("Sent!");
        }
        
        socket.close();
	}

}
