package com.sarta.android;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;

import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.os.Handler;
import android.util.Log;


public class AudioRecordThread extends Thread{

	static final String TAG = "AudioThread";

	private AudioRecord audioRecord;
	private DatagramChannel audioConnection;

	//inputs
	Handler ui;
	int sampleTime;
	InetAddress targetIP;
	int port;

	//Audio Config	
	int audioFrequency = 44100; //Hz
	int audioBufferSize;
	final int channelConfiguration = AudioFormat.CHANNEL_IN_MONO;
	final int audioEncoding = AudioFormat.ENCODING_PCM_16BIT;
	final int audioSource = MediaRecorder.AudioSource.MIC;

	private static final int sizeOfLong = Long.SIZE/8;
	private static final int sizeOfShort = Short.SIZE/8; 


	protected AudioRecordThread(Handler ui, InetAddress targetIP, int port, int sampleTime){

		super("AudioRecordThread");
		this.setDaemon(true);
		this.ui = ui;
		this.targetIP = targetIP;
		this.port = port;
		this.sampleTime = sampleTime;
		this.start();

	}


	public void run() {

		//		//get supported rates/buffers
		//        for (int tmpFrequency: new int[] {8000, 11025, 16000, 22050, 44100}) {  // add the rates you wish to check against
		//            int tmpBufferSize = AudioRecord.getMinBufferSize(tmpFrequency, channelConfiguration, audioEncoding);
		//            StringBuilder message = new StringBuilder().append(tmpFrequency).append(" Hz: ");
		//            if (tmpBufferSize > 0) {
		//                // buffer size is valid, Sample rate supported
		//            	message.append("minbuffer = " + tmpBufferSize);
		//            	audioFrequency = tmpFrequency;
		//            	audioBufferSize = tmpBufferSize;	                	
		//            }else{
		//            	message.append("not supported!");
		//            }
		//            Log.v(TAG, message.toString());
		//        }


		//setup recording machinery
		audioBufferSize = AudioRecord.getMinBufferSize(audioFrequency, channelConfiguration, audioEncoding);
		audioRecord = new AudioRecord(audioSource, audioFrequency, channelConfiguration, audioEncoding, audioBufferSize);
		int sampleFrames = audioRecord.getSampleRate() * sampleTime / 1000;
		audioRecord.startRecording();
		//initialize outbound channel
		try {
			audioConnection = DatagramChannel.open().connect(new InetSocketAddress(targetIP, port));
		} catch (Exception e) {
			Log.e(TAG, "Cannot make outbound channel");
		}

		ByteBuffer packetData = ByteBuffer.allocateDirect(sizeOfLong + sizeOfShort * sampleFrames);
		
		//main thread loop
		while(!isInterrupted()){

			if(!audioConnection.isConnected()){
				continue;
			}
			packetData.rewind();
			
			Log.v(TAG, "recorded bytes = " + audioRecord.read(packetData, sizeOfShort * sampleFrames));
			try {
				Log.v(TAG, "sent bytes = " + audioConnection.write(packetData));
			} catch (IOException e) {
				Log.e(TAG, "Error sending bytes" + e.getMessage());
			}

			packetData.putLong(packetData.limit() - sizeOfLong, System.nanoTime());
			Log.v(TAG, "timestamp - " + packetData.getLong(packetData.limit() - sizeOfLong));
			
			
		}
		audioRecord.stop();
		audioRecord.release();
		try {
			audioConnection.close();
		} catch (IOException e) {
			Log.e(TAG, "Error closing connection");
		}

	}
}
