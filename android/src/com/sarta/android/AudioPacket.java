package com.sarta.android;

import java.net.DatagramPacket;
import java.net.InetAddress;
import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;

public class AudioPacket {
	private static final int sizeOfLong = Long.SIZE/8;
	private static final int sizeOfShort = Short.SIZE/8; 

	private DatagramPacket datagramPacket;
	private byte[] audioData;
	private long timeStamp;

	//gets audio samples from packet (PC - byte array)
	public AudioPacket(DatagramPacket datagramPacket) {

		ByteBuffer buffer = ByteBuffer.wrap(datagramPacket.getData());
		try{
			this.timeStamp = buffer.getLong();
			this.audioData = new byte[buffer.remaining()];
			buffer.get(audioData);

		}catch (BufferUnderflowException e){

		}	
	}

	//builds packet from audio samples (Android - short array)
	public AudioPacket(short[] audioBuffer, InetAddress targetIP, int port, long timeStamp) {
		int bufferSize = sizeOfLong + sizeOfShort * audioBuffer.length;
		ByteBuffer buffer = ByteBuffer.allocate(bufferSize);
		try{
			buffer.putLong(timeStamp);
			for (short sample : audioBuffer){
				buffer.putShort(sample);			
			}
		}catch (BufferOverflowException e){

		}

		this.datagramPacket = new DatagramPacket(buffer.array(), buffer.array().length, targetIP, port);
	}

	public long getTimeStamp() {
		return timeStamp;
	}

	public byte[] getData() {
		return audioData;
	}

	public DatagramPacket getDatagramPacket() {
		return datagramPacket;
	}

}
