package com.sarta.android;

import java.net.InetAddress;
import java.net.UnknownHostException;

import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class AudioActivity extends Activity {

	private static final String TAG = "AudioActivity";
	Handler ui;

	//network config
	InetAddress targetIP;
	int port = 1355;

	//audio config
	final int sampleTime_ms = 500; // in ms

	//Threads
	AudioRecordThread audioRecordThread;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_audio);

		try {
			targetIP =InetAddress.getByName("192.168.43.240");
		} catch (UnknownHostException e) {
			Log.e(TAG, e.getMessage());
			e.printStackTrace();
		}
		Button button_start = (Button)findViewById(R.id.button_start);
		button_start.setOnClickListener(new Button.OnClickListener(){

			public void onClick(View v) {

				//Setup audio recording
				if(audioRecordThread == null){
					audioRecordThread = new AudioRecordThread(ui, targetIP, port, sampleTime_ms);				
				}
			}

		});

		Button button_stop = (Button)findViewById(R.id.button_stop);
		button_stop.setOnClickListener(new Button.OnClickListener(){

			public void onClick(View v) {

				if(audioRecordThread != null) {
					audioRecordThread.interrupt();
					audioRecordThread = null;
				}

			}

		});


	}

}
