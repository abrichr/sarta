package com.sarta.lib;

import java.nio.ByteBuffer;
import java.util.Arrays;

/**
 * Utility class for Sarta
 * @author richie
 *
 */
public class Utils {

	public static final int sizeOfLong = Long.SIZE/8; 
	public static final int BUFFER_SIZE = 65507;
	
	/**
	 * Returns a byte[] representation of a long.
	 * 
	 * @param 	l	The long to be converted
	 * @return		The byte[] representation
	 */
	public static byte[] longToBytes(long l) {
		byte b[] = new byte[sizeOfLong];
		ByteBuffer buf = ByteBuffer.wrap(b);
		buf.putLong(l);
		return b;
	}

	/**
	 * Returns a long representation of a byte[].
	 * @param 	b	The byte[] to be converted
	 * @return		The long representation
	 */
	public static long bytesToLong(byte[] b) {
		ByteBuffer buf = ByteBuffer.wrap(b);
		return buf.getLong();
	}	
	
	/**
	 * Concatenates two byte arrays together (in order).
	 * @param 	a	The leftmost byte array
	 * @param 	b	The rightmost byte array
	 * @return		The combined byte arrays
	 */
	public static byte[] catBytes(byte[] a, byte[] b) {
		byte[] c = new byte[a.length + b.length];
		System.arraycopy(a, 0, c, 0, a.length);
		System.arraycopy(b, 0, c, a.length, b.length);
		return c;
	}
	
	/**
	 * Concatenates time stamp to the front of byte array.
	 * @param 	dataBytes	Byte array containing data	
	 * @return				Byte array containing timestamp and data
	 */
	public static byte[] addTimeBytes(byte[] dataBytes) {
		Long timeStamp = System.nanoTime();
		byte[] timeBytes = longToBytes(timeStamp);

		byte[] buffer = catBytes(timeBytes, dataBytes);
		return buffer;
	}

	/**
	 * Gets the time stamp from a byte array
	 * @param buffer	The byte array
	 * @return			The time stamp
	 */
	public static long getTimeStamp(byte[] buffer) {
		byte[] timeBytes = Arrays.copyOfRange(buffer, 0, sizeOfLong);
		long timeStamp = bytesToLong(timeBytes);
		return timeStamp;
	}
	
	/**
	 * Removes the time stamp from a byte array
	 * @param buffer	The byte array
	 * @return			The byte array with the leading timestamp removed
	 */
	public static byte[] stripTimeBytes(byte[] buffer) {
		int bufferSize = buffer.length;
		byte[] dataBytes = Arrays.copyOfRange(buffer,sizeOfLong,bufferSize);
		return dataBytes;
	}

	/**
	 * Splits a byte array into smaller byte arrays of a given size
	 * @param src		The byte array to be split
	 * @param newSize	The maximum size of the new byte arrays
	 * @return			An array of byte arrays of the given size (or smaller)
	 */
    public static byte[][] splitByteArray(byte[] src, int newSize) { 
    	int oldSize = src.length;
    	System.out.println("oldsize: " + oldSize);
    	int numArrs = (int) Math.ceil((double)oldSize / newSize);
    	int lastSize = oldSize - (numArrs-1) * newSize;
    	byte[][] result = new byte[numArrs][];
    	for (int i = 0; i < numArrs; i++) {
    		byte[] thisArray = new byte[newSize]; 
    		int length = (i < numArrs-1) ? newSize :  lastSize;
    		System.arraycopy(src, i*newSize, thisArray, 0, length); 
    		result[i] = thisArray;     				
    	}
    	return result;
    }	
}
